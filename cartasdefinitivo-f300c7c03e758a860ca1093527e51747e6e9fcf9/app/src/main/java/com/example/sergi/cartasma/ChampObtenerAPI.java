package com.example.sergi.cartasma;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Sergi on 16/11/2017.
 */

public class ChampObtenerAPI {
    private final String BASE_URL = "https://euw1.api.riotgames.com/lol/static-data/v3/champions?locale=es_ES&dataById=true&api_key=RGAPI-003d2759-6aad-462d-bf37-139c3a1035a5";

    ArrayList<Card> getCards() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .build();
        String url = builtUri.toString();

        try {
            Log.d("COSAS", HttpUtils.get(url));
            String JsonResponse = HttpUtils.get(url);
            return proccesJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Card> proccesJson(String jsonResponse) {
        ArrayList<Card> cards =new ArrayList<>();
        try {
            JSONObject mainJson = new JSONObject(jsonResponse);
            JSONObject data = mainJson.getJSONObject("data");
            Iterator it = data.keys();
            while(it.hasNext()) {
                Object key = it.next();
                JSONObject jsonCard = data.getJSONObject((String) key);
                Log.d("COSAS", jsonCard.getString("name"));
                Card card = new Card();
                card.setNombre(jsonCard.getString("name"));
                card.setImageUrl("http://ddragon.leagueoflegends.com/cdn/8.11.1/img/champion/"+jsonCard.getString("key")+".png");
                card.setDescripcion(jsonCard.getString("title"));

                cards.add(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cards;
    }
}
