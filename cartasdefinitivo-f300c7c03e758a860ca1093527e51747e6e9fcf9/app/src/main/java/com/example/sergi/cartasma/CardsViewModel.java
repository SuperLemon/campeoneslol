package com.example.sergi.cartasma;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CardsViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CardDao cardDao;
    private LiveData<List<Card>> cards;

    public CardsViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cardDao = appDatabase.getCardDao();
    }

    public LiveData<List<Card>> getCards() {

        return cardDao.getCards();
    }
    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {
            ChampObtenerAPI api = new ChampObtenerAPI();
            ArrayList<Card> result = api.getCards();

            Log.d("DEBUG", result.toString());

            cardDao.deleteCards();
            cardDao.addCards(result);

            return result;
        }

    }

}
