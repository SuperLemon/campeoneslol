package com.example.sergi.cartasma;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sergi.cartasma.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;
    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            Card card = (Card) i.getSerializableExtra("card");

            if (card != null) {
                updateUi(card);
            }
        }
        return view;
    }

    private void updateUi(Card card) {
        Log.d("CARTA",card.toString());

        binding.tvTitle.setText(card.getNombre());
        binding.tvDescripcion.setText(card.getDescripcion());

        Glide.with(getContext()).load(
                card.getImageUrl()
        ).into(binding.imageView);
    }
}
